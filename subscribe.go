package rediscli

import (
	"gitee.com/zhengfangxin/log"
	"github.com/garyburd/redigo/redis"
	"time"
)

type Subscribe struct {
	pool *redis.Pool
	chans []string
	notify func(string, []byte)
}

func (e *Subscribe) Init(addr, passwd string, chans []string, notify func(string, []byte) ) error {
	e.chans = chans
	e.notify = notify

	pool := &redis.Pool{
		MaxIdle:     1,
		MaxActive:   1,
		Wait:        false,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			passwdopt := redis.DialPassword(passwd)
			return redis.Dial("tcp", addr, passwdopt)
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}

	conn := pool.Get()
	defer conn.Close()
	err := conn.Err()
	if err != nil {
		return err
	}

	e.pool = pool

	go e.loop_subscribe()

	return nil
}

func (e Subscribe) loop_subscribe() {
	for {
		e.subscribe()
		time.Sleep(time.Second)
	}
}
func (e Subscribe) subscribe() {
	conn := e.pool.Get()
	defer conn.Close()

	psc := redis.PubSubConn{Conn: conn}
	for _,cur := range e.chans {
		psc.Subscribe(cur)
	}

loop:
	for {
		switch n := psc.Receive().(type) {
		case redis.Message:
			channel := n.Channel
			e.notify(channel, n.Data)

		case redis.PMessage:
			log.Info("PMessage: %s %s %s\n", n.Pattern, n.Channel, n.Data)
		case redis.Subscription:
			log.Info("redis Subscription: %s %s %d\n", n.Kind, n.Channel, n.Count)
			if n.Count == 0 {
				continue
			}
		case error:
			log.Error("redis push recv error: %v\n", n)
			break loop
		}
	}
}